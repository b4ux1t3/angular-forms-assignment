import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Customer } from './Customer';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  submitted: boolean = false;
  customer: Customer;
  subscriptions: string[] = ['Basic', 'Advanced', 'Professional'];
  defaultSelection = this.subscriptions[1].toLowerCase();;
  @ViewChild('signupForm') signupForm: NgForm;



  onSubmit(): void {
    this.customer = new Customer();
    this.customer.email = this.signupForm.value.email;
    this.customer.password = this.signupForm.value.pass;
    this.customer.subscription = this.signupForm.value.subscriptionType
    console.log(this.signupForm.value);
    this.submitted = true;
    this.signupForm.reset();
  }

  capitalizeFirstLetter(inp: string): string {
    return inp.charAt(0).toUpperCase() + inp.slice(1);
  }
}
